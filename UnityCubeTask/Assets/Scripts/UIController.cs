using UnityEngine;

public class UIController : MonoBehaviour
{
    public CubeController Cube;
    public GameObject RestartUI;
    public GameObject[] Targets;

    public void Restart()
    {
        RestartUI.gameObject.SetActive(false);
        Cube.Restart();
        foreach (var collectableSphere in Targets)
        {
            collectableSphere.gameObject.SetActive(true);
        }
    }

    public void Finish()
    {
        RestartUI.gameObject.SetActive(true);
    }
}