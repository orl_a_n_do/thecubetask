using UnityEngine;
using UnityEngine.UI;

public class CubeController : MonoBehaviour
{
    public Text CountText;
    public UIController UIController;

    private Vector3 _mouseStartPosition;
    private Camera _camera;
    private int _count;
    private Vector3 _startPosition;

    private void Start()
    {
        _camera = Camera.main;
        _startPosition = transform.position;
    }

    private void OnMouseDown()
    {
        _mouseStartPosition = gameObject.transform.position - _camera.ScreenToWorldPoint(MouseInputPosition());
    }

    private void OnMouseDrag()
    {
        transform.position = _camera.ScreenToWorldPoint(MouseInputPosition()) + _mouseStartPosition;
    }

    private Vector3 MouseInputPosition()
    {
        return Input.mousePosition + new Vector3(0f, 0f, 12f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Spheres"))
        {
            other.gameObject.SetActive(false);
            _count++;
            UpdateCount();
        }

        if (_count == 5)
        {
            UIController.Finish();
        }
    }

    private void UpdateCount()
    {
        CountText.text = $"Count: {_count}";
    }

    public void Restart()
    {
        _count = 0;
        UpdateCount();
        transform.position = _startPosition;
    }
}